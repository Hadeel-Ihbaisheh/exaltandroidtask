package com.example.exalt_project;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class SignUP extends AppCompatActivity {
    public static final String SHARED_PREFERENCE_NAME = "Members";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        final EditText fNameEditText = findViewById(R.id.fName);
        final EditText lNameEditText = findViewById(R.id.lName);
        final EditText emailEditText = findViewById(R.id.emailSignup);
        final EditText passwordEditText = findViewById(R.id.passwordSignup);
        final Button registerButton = findViewById(R.id.registerButton);

        registerButton.setEnabled(false);



        TextWatcher mTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                checkRegisterInput();
            }
            public void  checkRegisterInput() {
                ValidationClass validationClass =new ValidationClass();

                String fName = fNameEditText.getText().toString();
                String lName = lNameEditText.getText().toString();
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();


                boolean validEmail= validationClass.isValidEmail(email);
                boolean validPassword= validationClass.isValidPassword(password);
                boolean validFname= validationClass.isValidName(fName);
                boolean validLname= validationClass.isValidName(lName);

                if(validFname){
                    fNameEditText.setError(null);
                }
                else{
                    fNameEditText.setError(" first name must be more than 2 characters !");
                }

                if(validLname){
                    lNameEditText.setError(null);
                }
                else{
                    lNameEditText.setError(" last name must be more than 2 characters !");
                }

                if(validEmail){
                    emailEditText.setError(null);
                }
                else{
                    emailEditText.setError(" please enter a valid email address !");
                }
                if(validPassword){
                    passwordEditText.setError(null);
                }
                else{
                    passwordEditText.setError("Password must be more that 6 characters !");
                }



                if(validFname && validLname && validEmail && validPassword ){
                    registerButton.setEnabled(true);
                    registerButton.setTextColor(getResources().getColor(R.color.whiteColor));
                }
                else{
                    registerButton.setEnabled(false);
                    registerButton.setTextColor(getResources().getColor(R.color.greyColor));
                }


            }

        };


        fNameEditText.addTextChangedListener(mTextWatcher);
        lNameEditText.addTextChangedListener(mTextWatcher);
        emailEditText.addTextChangedListener(mTextWatcher);
        passwordEditText.addTextChangedListener(mTextWatcher);




        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fName = fNameEditText.getText().toString();
                String lName = lNameEditText.getText().toString();
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();


                ProgressDialog dialog = new ProgressDialog(SignUP.this);
                dialog.setMessage("Creating account..");
                dialog.show();

                SharedPreferences.Editor editor = getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE).edit();

                editor.putString("fName", fName);
                editor.putString("lName", lName);
                editor.putString("email", email);
                editor.putString("password", password);
                editor.apply();
                Toast.makeText(getApplicationContext(), "Registered Successfully", Toast.LENGTH_SHORT).show();
                goToLogIn(view);
            }
        });




    }






    public void goToLogIn(View view) {
        Intent loginIntent = new Intent(getBaseContext(), Login.class);
        startActivity(loginIntent);
    }
}
