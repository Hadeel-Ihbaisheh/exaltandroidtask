package com.example.exalt_project;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    public static final String SHARED_PREFERENCE_NAME = "Members";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        final EditText emailEditText = findViewById(R.id.emailLogin);
        final EditText passwordEditText = findViewById(R.id.passwordLogin);
        final Button loginButton = findViewById(R.id.loginButton);

        loginButton.setEnabled(false);



        TextWatcher mTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                validateLogInInput();
            }



            public void  validateLogInInput() {
                ValidationClass validationClass =new ValidationClass();

                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();


                boolean validEmail= validationClass.isValidEmail(email);
                boolean validPassword= validationClass.isValidPassword(password);

                if(validEmail){
                    emailEditText.setError(null);
                }
                else{
                    emailEditText.setError(" please enter a valid email address !");
                }
                if(validPassword){
                    passwordEditText.setError(null);
                }
                else{
                    passwordEditText.setError("Password must be more that 6 characters !");
                }
                if(validEmail && validPassword){
                    loginButton.setEnabled(true);
                    loginButton.setTextColor(getResources().getColor(R.color.whiteColor));
                }
                else{
                    loginButton.setEnabled(false);
                    loginButton.setTextColor(getResources().getColor(R.color.greyColor));
                }



            }



        };



        emailEditText.addTextChangedListener(mTextWatcher);
        passwordEditText.addTextChangedListener(mTextWatcher);




        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE);


                String memberEmail = prefs.getString("email", "No email ");
                String memberPassword = prefs.getString("password", "No password ");


                String enteredEmail = emailEditText.getText().toString();
                String enteredPassword = passwordEditText.getText().toString();


                ProgressDialog dialog = new ProgressDialog(Login.this);
                dialog.setMessage("wait..");
                dialog.show();

                if(memberEmail.equals(enteredEmail) && memberPassword.equals(enteredPassword)){
                    String memberFName = prefs.getString("fName", "No first Name ");
                    String memberLName = prefs.getString("lName", "No last Name ");
                    Toast.makeText(getApplicationContext(), "Welcome "+memberFName, Toast.LENGTH_LONG).show();



                    goToDashboard(memberFName,memberLName);


                }
                else{
                    Toast.makeText(getApplicationContext(), "email / password are incorrect ! ", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }

            }

            private void goToDashboard(String memberFName,String memberLName) {
                Intent dashboardIntent = new Intent(getBaseContext(), Dashboard.class);
                dashboardIntent.putExtra("fName", memberFName);
                dashboardIntent.putExtra("lName", memberLName);
                startActivity(dashboardIntent);
            }


        });




    }
    public void goToSignup(View view) {
        Intent signupIntent = new Intent(getBaseContext(),  SignUP.class);
        startActivity(signupIntent);
    }




}
