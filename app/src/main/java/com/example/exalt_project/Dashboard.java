package com.example.exalt_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Dashboard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

final Button logOutButton =findViewById(R.id.logoutButton);

        final TextView welcomeView = findViewById(R.id.welcomeView);
        String fName = getIntent().getStringExtra("fName");
        String lName = getIntent().getStringExtra("lName");
        String helloString = "Hello "+fName+" "+lName;
        welcomeView.setText(helloString);



        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent =  new Intent(getBaseContext(), Login.class);
                startActivity(loginIntent);
            }});





    }
}
