package com.example.exalt_project;

public class ValidationClass {


    public  boolean isValidPassword(String password){

        if (password.isEmpty() || password.length() < 6 ) {
            return false;}
        else {
            return true;}

    }

    public  boolean isValidEmail(String email){

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return false;}
        else {
            return true;}

    }

    public  boolean isValidName(String name){

        if (name.isEmpty() || name.length() < 2 ) {
            return false;}
        else {
            return true;}

    }





}
